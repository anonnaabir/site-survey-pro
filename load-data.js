import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Image, Modal, Alert, TouchableOpacity} from 'react-native';
import { Container, Content, Text, Button, Icon, Item, Input, Footer, FooterTab} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import { ListItem } from 'react-native-elements';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Share from 'react-native-share';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'appdata.db' });

export default class LoadData extends Component {
  constructor(props) {
    super(props);
    this.array = [
    ],
    
      this.state = {
        arrayHolder: [],
        location: '', note: '', image: '', imageData: '',
        dataid: '',
        filePath: '',
        modalVisible: false,
        editmodalVisible: false,
        projectModal: true,
        projectName: null,
        projectNote: '',
        //flotactive: false,
        projectNameLabel: null,
        projectNoteLabel: null,
        field01Label: null,
        field02Label: null,
        companyLogo: null,
        createdBy: null,
        sendEmailSubject: null,
        sendEmailBody: null,
      }

      this.readData();
  }


  saveData = () => {
    console.log('Insert Start');
    //var that = this;
    const {projectName} = this.state;
    const { location } = this.state;
    const { note } = this.state;
    const { image } = this.state;
    const { imageData } = this.state;
          db.transaction(function(tx) {
            tx.executeSql(
              'INSERT INTO ' + projectName + ' (location, note, image, imagedata) VALUES (?,?,?,?)',
              [location, note, image, imageData],
              (tx, results) => {
                // console.log(results);
                // console.log('Row ID', results.insertId);
                // console.log(dataid);
                console.log('Database Worked');
              }
            );
          });
          
          Alert.alert(
            'Done',
            'Data Saved Successfully',
            [
              {text: 'OK', onPress: () => {this.setModalVisible(false), this.readData()}},
            ],
            { cancelable: false }
          )
        };


        editData = () => {
          console.log('Edit Start');
          const {dataid} = this.state;
          //var that = this;
          const {projectName} = this.state;
          const { location } = this.state;
          const { note } = this.state;
          const { image } = this.state;
          const { imageData } = this.state;
                db.transaction(function(tx) {
                  tx.executeSql(
                    "UPDATE " + projectName + " SET location='" + location + "',note='" + note + "' ,image='" + image + "' ,imageData='" + imageData +  "' WHERE Id=" + dataid,
                    // [location, note, image, imageData],
                    (tx, results) => {
                      console.log('Results', results.id);
                      console.log('Database Worked');
                    }
                  );
                });
                
                Alert.alert(
                  'Done',
                  'Data Saved Successfully',
                  [
                    {text: 'OK', onPress: () => {this.editModalVisible(false), this.readData()}},
                  ],
                  { cancelable: false }
                )
              };


  setModalVisible = (visible) => {
      this.setState({ modalVisible: visible });
    }

    editModalVisible = (visible) => {
      this.setState({ editmodalVisible: visible });
    }


  readData = () => {
    const {projectName} = this.state;
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM ' + projectName, [], (tx, results) => {
        var data = [];
        for (let i = 0; i < results.rows.length; ++i) {
          data.push(results.rows.item(i));
        }
        this.setState({arrayHolder: data});
        console.log('Read Done');
      });
    });
  }



  getProjectSettings = async () => {
    try {
      const name = await AsyncStorage.getItem('ProjectNameSet');
      const note = await AsyncStorage.getItem('ProjectNoteSet');
      if (name!==null || note!==null) {
        this.setState({projectNameLabel: name});
        this.setState({projectNoteLabel: note});
      }

      else {
        this.setState({projectNameLabel: 'Project Name'});
        this.setState({projectNoteLabel: 'Project Notes'});
      }

    } catch (error) {
      console.log('Data Read Error');
    }
  };


  getFieldSettings = async () => {
    try {
      const field1 = await AsyncStorage.getItem('Field01');
      const field2 = await AsyncStorage.getItem('Field02');
      if (field1!==null || field2!==null) {
        this.setState({field01Label: field1});
        this.setState({field02Label: field2});
      }

      else {
        this.setState({field01Label: 'Location'});
        this.setState({field02Label: 'Notes'});
      }
      // console.log('Data Received:' + field1);

    } catch (error) {
      console.log('Data Read Error');
    }
  };


  getReportSettings = async () => {
    try {
      const logo = await AsyncStorage.getItem('CompanyLogo');
      const createby = await AsyncStorage.getItem('CreatedBy');
      if (logo!==null || createby!==null) {
        this.setState({companyLogo: logo});
        this.setState({createdBy: createby});
      }

      else {
        this.setState({companyLogo: ''});
        this.setState({createdBy: 'Site Survey Pro'});
      }
      // console.log('Data Received:' + field1);

    } catch (error) {
      console.log('Data Read Error');
    }
  };


  getEmailSettings = async () => {
    try {
      const subject = await AsyncStorage.getItem('EmailSubject');
      const body = await AsyncStorage.getItem('EmailBody');
      if (subject!==null || body!==null) {
        this.setState({sendEmailSubject: subject});
        this.setState({sendEmailBody: body});
      }

      else {
        this.setState({sendEmailSubject: 'Here is the report from'});
        this.setState({sendEmailBody: 'Please check the attachment for the PDF Report.'});
      }
      // console.log('Data Received:' + field1);

    } catch (error) {
      console.log('Data Read Error');
    }
  };

  componentDidMount() {
    this.setState({ arrayHolder: [...this.array] });
    const { navigation, route} = this.props;
    const {oldprojectName} = route.params;
    this.setState({projectName: oldprojectName});
    this.getProjectSettings();
    this.getFieldSettings();
    this.getEmailSettings();
    this.getReportSettings();
    console.log('Project Received ' + oldprojectName);
  }

  chooseImage = () => {
    ImagePicker.openPicker({
      width: 150,
      height: 200,
      multiple: true,
      // cropping: true,
      includeBase64: true,
    }).then(image => {
      this.onSelectedImage(image);
      this.imageData(image);
      console.log(image.data);
    });
  }

  captureImage = () => {
    ImagePicker.openCamera({
      width: 150,
      height: 200,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      this.onSelectedImage(image);
      this.imageData(image);
      console.log(image);
    });
  }

  onSelectedImage = (image) => {
    console.log(image.path);
    console.log(image.data);
    this.setState({image: image.path});
  }

  imageData = (image) => {
    console.log(image.data);
    this.setState({imageData: image.data});
  }

  ListEmpty = () => {
      return (
      <Container>
      <Content contentContainerStyle={styles.content}>
        <Text
        style={{
          textAlign: 'center',
          paddingLeft: 30,
          paddingRight: 30,
          paddingBottom: 10
        }}
      >Locations Not Lodaed Yet. Please, click on the button to load locations.</Text>
      <Button
      style={{
        backgroundColor: '#2B2D2F',
      }}
      >
          <Text
          onPress={this.readData}
          >Load Locations</Text>
        </Button>
        </Content>
      </Container>
    );
  };
  
  
  createPDF = async() => {
    const { arrayHolder } = this.state;
    let reportOutput = arrayHolder.map((i) => {
      console.log(i);
      return`<tr>
      <td>${i.location}</td>
      <td>${i.note}</td>
      <td><img src=${`data:image/jpg;base64,${i.imagedata}`} /></td>
      </tr>
      `;
   });
    
    const {projectNameLabel} = this.state;
    const {projectNoteLabel} = this.state; 
    const {projectName} = this.state;
    const {projectNote} = this.state;
    const {field01Label} = this.state;
    const {field02Label} = this.state;
    const {createdBy} = this.state;
    const {companyLogo} = this.state;

    let defaultLogo = 'iVBORw0KGgoAAAANSUhEUgAAAMgAAAC0CAYAAADRuGMkAAAt2UlEQVR4AezBB4BcZaEw7Gdmd7NpJAOEFgicpSNVelF5jwJiAxSGJghiBfGKcMWCBRWxIliwgAgISBkLCBhU8Ly0q4iAIEqRcughoUwaySa7O3/+u3x3GDYV2M3sZp+nYFi/CSEtYE1siQ2wPiZhTayDkVgTLfqajycwF0/gcTyGR/Fv3BNj9pxh/apg2GsmhHQCdsR22Ak7YjxaUETBq1dDN3rwBG7HzbgVt8eYvWDYa6Zg2CsWQtqKLfEWvBXbYSxaUTBwapiHKm7ANbghxuwBw16VgmHLJIS0iM2xL/bHhhiDgubRjZm4Db/G1TFmjxq2zAqGLZUQ0vF4J47EdhiPolevB8+hW107Sl4b3ZiGP+KX+HOM2XzDlkrBsEUKIbVAggPxAXSgzbKZjofwBB7BA3gc0zAF3ehEj7oWjEAbJmF1TMIGWAcJJmG0ZfMC7sJPcXmMWdWwxSoY1kcIaQEJjsKRmIiiJevG47gXf8XNeBjPY2aM2TyvQgipBUaihPHYCjtjG2yB1VCwZN24B9/Hr2PMnjNsoQqGNQghXQtH4qNY15LNwT24EX/AvzElxqzTAAkhHY218Xq8HTtjA7RavG7cjdNwRYzZDMMaFAz7XyGkY7A//htboGDRuvEwJuNy3Blj9qwmEEJaxFrYAQdjN6xj8ebhJnwVN8WYdRn2vwpWcCGkLdgaJ+OtGGHRZuMWXIDr8ESMWY8mFULagg2xLw7B6zDColVxIU7HwzFmNSu4ghVYCGkJH8FxWNOizcR1+BFujjF7wSATQjoBb8Wx2BYjLNq/8GVcGWM21wqsYAUUQlrAlvg69kKrhXsB1+IM3BRjNt8gF0I6Du/EJ7ANRli4F3Auvh5j9oQVVMEKJoR0BPbDN5FYuC7cilMxOcas2xATQjoGh+BT2AgFC3cLjsdfY8x6rGAKViAhpONwIj6J0fqq4Ql8F2fFmM02xIWQroUT8X6Mt3BP4zP4ZYzZPCuQghVECOna+AH2QYu+OjEZJ8aY/ccKJoT0jfg2tkeLvubi+/hKjNlsK4gWK4AQ0k1xMfZAUaMapuFTOCnGbJoVUJ7njyZJx6UYga0xQqNW7IyNk6TjujzP51oBFAxhIaQW2B4XY0N91XAzjo4xu9uw/xVC+i6ciUn6qiHivTFmTxniWgxRIaQW2BlXYx199eAsHBlj9rhh/yfP8/uTpOM32AodGhXQgd2TpOOKPM9nG8JaDFFJ0rELfo9V9TUTJ+LkGLNOw/rI83x6knT8FuOxHQoaTcQeSdLx2zzPZxuiCoaYEFIL7IzfY2V9PY2P4ooYs5oBVq0kWyDFnvhaqZzfYiGqlWQCfoJHcR1uKJXzmQZYCOlIfAJfRru+7sDeMWZTDUGthp5NUcHK+noE70eMMavpZ9VKYoG1sTN2w95I0I4izrB4G2N/HIsZ1UpyPa7EH/BUqZzrbzFmc0NIv4tpOB3jNHo9fhdCuleM2QxDTNEQEkI6Cb/BOvp6AO9GjDGr6WfVSjICJ+MfuBifxGYYhaJl04ZV8R78DHfhvGolWcUAiDGbj/NwNGboa0dcEEI6yhBTNESEkK6Kc7GZvh7AgfhHjFnNwHgTPocJaPPaacEEHIFjqpWkYADEmPXgUhyNGRoV8E6cGkLaZggpGgJCSNtxCt6sr6dxGP4RY1YzcLZDm/71erQZIDFm3bgUx6BToyKOwVEhpEVDRKtBLoS0BR/GUShoNAOH428xZjX9oFpJCtgEU0rlvKquVf8replZNxyxW9fT199aKufz9IMYs+4Q0kuxBr6NoroR+AbuR2YIKBr8dsWXMUKjeTgW18WY1fSDaiUpYH+cgwmaQG3Ok6fg89VKMlY/iTHrwg/xfX2VcEYI6SRDQNEgFkK6Nr6DlfX1bVwcY9ajH1QrSQH744dYR1/PWy5qY/AZnF6tJBP0kxizefgirtTXVvhyCOkog1zRIBVCOgInYEd9XYWvxph16QfVSlLAe/BDrGHhqpZsrEUrotUr04b34wfVSjJeP4kxm4ljcL++DsWhIaQFg1jR4LUXPqSvh3BMjFmn/pPiB1jDq7OJRRuHNb1yLTgAp1QryUj953F8FDM1asdJ2NggVjQIhZCuhS9grEaz8V8xZo/pJ9VKsj5+gLW8em+vVpLxXqZaSYrYEyt7dVrxIXy4WkkK+kGMmRizDN9Bj0Yd+HQI6UiDVNEgE0JaxEewvUY9+GmM2dX6SbWSjMA3sZnXxhvwqWolWb1aSQoWqFaS0dgTn/faaMdJ2En/+g5u1tdBeJtBqmjw2RwfRlGju/E1/etAvAsFS9ZqyVrxWVyLC6qV5Dz8Fr/GRK+d1XFKtZKM0k9izF7Af+M5jUbjxBDSCQahokEkhLQNn8JaGs3GF2PMntNPqpVkJXwB7ZbOJEuniC3xXhyBvTDGK1Tr6bYIb8R++lGM2d/wQ/RotCMODSE12BQNLjtjH339DlfpX4dhA02uZ9bDFmEEjq9WkqL+dQbu1qiIY7CuQaZokAghbcXHMV6jafhajFm3flKtJAUcgxaD2xZ4i34UY/Y8TsVcjTbB+0JIDSZFg8cu2Etf58WY/Uv/ehM2NviNxL76329xg76OwroGkaJBIIS0BcdhvEYP4wz9by+M0HxWQotl8y79LMZsHr6D2Rp14ACDSNHgsA1216iGs/Gk/hcs3hp4nRdVK8kovEH/2xKreVG1kqyLCRZvtWoleZ3+l+F6fR0VQrqyQaLV4HA4VtXoCVRizPSnaiVpx7YWrx1nVCvJKngEB2Iv/W91XFCtJN9CNz6LxOK1YQv8Wz+KMesKIf0BdscYdRvj7bjIINCqyYWQrol99XUJHtT/JqHFknXgLHRhJAoGxpuwk17tlqwVHQbG9fg7dlfXhsNCSC+OMevR5Iqa395YW6NncEmMWU3/WwtFS6cNo1AwsNrRbumtYwDEmM3BeejSaCe83iBQ1MRCSIs4FG0a/QV3GhijDT3jDZw/4AGNSniXQaCouW2K7TTqwgUxZl0GxmooGFrWNHCmYLJGBewXQjpakytqbnuipFGOvxo4rSgYWhIDJMashssxS6P1sa0mV9Tc3oaiRtfhCQOgWkkssCkKhpZ1q5VkooFzJ+7RaCzerMkVNakQ0rWxnUbdmBxj1mNgjMR7DD0jcEC1khgIMWbTca1GBbwjhLSoiRU1r10xXqMcdxgA1UrSguOwkaGngBOxhoEzGZ0abYz1NbGi5rUr2jS6G0/qZ9VKMg7H42RD19o4p1pJ1qpWEgPgP3hQo7HYURMral5v0td1MWZd+lG1kqyOc/ANtBva3oHJ2L1aSYr619O4U6NW7KqJFTWhENIJ2FCjubhFP6pWkjacggNQtGLYGr/BZvpRjFkNN+hre02sqDltg3aNHsOT+tcovN6KZxVsof/dgU6NNg0hXUWTKmpOG2OERg9hqv41B3+34pmFu/S/p/CERiOwsSZV1Jy2RUGje2PM5ulHpXI+H1/CFegx9NXwPD6I+/S/qcg1GoHNNKmi5rShvu4wAErlfCr2x1F43ND1PE7BlqVyfmmpnPfoZzFmc5Fr1IKNNKmi5rShvh4wQErlvBvnY2OcgEcNDTXcjWOxLr5YKudPGFgP6GsjTapVkwkhHYdVNJqOaQZQqZxbYE61knwXP8ZeOBABEw0uj2EyLsJfML9Uzi0n/9FXokm1aj5roKjRNMy2HJTKuQXmhJDOXm2lrhPPft9jVWyGXbEdXofXYbTm8ST+hb8hXn3XuFsv/8f4CdNmtu6FOTFmt1p+HtDX2ppUq+azOooaTcdcy9c3p81s7djvzI678BucffnHHj4TozAWG2FrbIJ10YG1MEH/mIun8AQeQ45/4F94EnPQud+ZHbvhUuyMdhyGWy0/czAD49SNDyFtiTHr1mRaNZ/VUNBoJjotX61YGbtjVVxcKufTMAuzMAU3VitJASMwCu1oR4JJWBWrYHWMRhGrokVfL2AGapiCOXgE0zAFT6MTczG3VM67LEQIHdvgrXrNt/zNw2yMU1fEWnhck2nVfMagoNF0zLEchJC2IMValkKpnNfQiU51j1qIaiUpoB0FfXWVyvl8r60W7BVCelOM2dOWjzmoYi2NippQq+bThoJG82LMug2wENISTsV7Mc5rrFTOa5hr4BTxQewWQvpJ/CnGzACroaZREWvhUU2mqPmsjqLlLIR0HfwGR2OcRjPRbXCYjfnqitgcFXw4hLRgYNXQrVEBIzShoubTYjkLIR2DHyHV6J/4JI5C1eDwG+yDSzBT3XicjreEkBpAc1A1SLQa1iCEtIDD8Q51L+Cn+A6ejDEzWMSYTcc1IaR/xh44FVvrNRpnYDdMNzAKKBokWjWfF1BDwfIxGu9HUa9ufAY/iTGbb5CKMZuH34eQ/hu/xTZ6bY498GsDYxRWNki0aj7PoActlo8x2EbdDfhpjNl8SymEtIBVsRrWwhjMxBQ8g2djzGqWUgjpKKyB1bCmXo/jWTwdY9Zp6eX4LCare0MI6a9jzCwnPXheE2o1OIwLIR0ZYzZX/ythhLq/Y56lFEI6Bh/DodgU7erm4j5cGkJ6RozZHIsRQlrArvgMdsAaGj2Bm0JIv4F/xJhZkhgzIaT/o9EqaEG3/teCERrVMEMTatV8nkVNo1Fow1z973l0ol2vHdGG+ZbOtjgVLfoaia2xFf6CaPHG4FS8ycKtjYPQgvdiniUIIbXArhpNiTHrNjBGYby+OjWhouYzAzWNVsEoA+MF3KbuDdgrhNRSugeXYjrmadSJ6Tgfd1myOfgpHsYc9KjrwQu4D2fHmM2zdMbgU+pquMnAacdKGnXFmD2tCbVqPo+hG23qShhlYLyAC7GrXi04E/eGkD4YY2ZxYsyeCSE9HOthS6yn7mH8Gw/HmNUsQYxZN34ZQnoFdsCGGKXXLNyH22LMOi2FENI2nIA3q7sf1xs4q2KkRlM0qVbNZyp6NFoFow2AGLNaCOmF+BBer9d6+BX2DyF9KMbM4sSY9eBhPOw1EGM2GxHRKxRC2oaP4YvqavgyZhg4ib4e06SKmkyMWSee0mg0JhogMWYz8SHMUrcN/oxdDTIhpKPxXZyOFnUX47IYMwNoQ33lmlRRc7pXX1saWLfhcHSpWw9/CCE9NoR0hEEghHQzTMaxGt2MD6LbwNpUXw9oUq2a0914h0ZbGEAxZkJIr8BhOAvj9BqL76GGMzWxENI1cAm2UlfDH3AI5sSY+X9CSFfFx7E97sKZMWZPeI2EkJaQaNSFBzWpouZ0P3o02jCEdIwBFGNWw2XYB7m6Io4PIe3QpEJIizgGW6rrwo+xf4xZNcbM/xNC2oYLsT/uRIoLQkhLXjurYKJG83CPJlXUnO7EXI06sLoBFmNWizG7Hm/BzerWx6dDSEdoTtvhIyjoNQcfw3ExZi94UQjpxBDSc/Ab7IwjcRI+hPVwcQjpz0NIt/DqJZio0Uzcq0m1ak73YhZGq1sH6+Fhy0GM2UMhpJ/C71HS6zBcg8s1kRDS8TgFa+hVw69wToxZdwjp2jgEY7EZ9sHl+CrujjETQnoPvoE3Yx+sEUJ6K2bj8hiz/1h2u6Go0T9jzOZpUkVNKMZsNu7WqIjU8nULzkaPXmPwlRDSRJMIIS3iI3izukfx1Riz7hDSibgSB2FTTEIVH4sx+26MWacFYsy6Y8zOxkfxINbCptgXV4aQbm4ZhJC2YWd9/VUTa9W8bsCbNXpTCOmoGLM5loMYs54Q0tOwF7bWawt8OYT06BizFyx/ASegVa9OfCvG7D96HYyReAOewx64HJ8OIZ2irwlYH8fGmF0aQroaLsH78GlLbxI216gTt2hiRc3rRszRaDOsZzmKMXsan8bzehVwED4cQlqwHIWQrofTsLpeNfwO56pbDzfHmD0XY2aBG/Ft7IaDcBAOwkE4CCl+gWssEGM2DQ9iA8tmc6yj0XTcoom1al634zmsrW51vAn3Wr6uxQ/wWbShHV/Aw7jCYoSQtmFNTIsxm2sJQkhHYjU8FWPWZRFCSFfG97G1untwUozZHI3me1GMWSe+HEL6HbTqqwezY8x6NCpaSiGkBbwTLRrdFmM2TRNr1aRizKohpDfgEHUF7BNCem6M2XzLSYxZdwjp6dgK+6KAVXBaCOnTMWZ/tWg74Sz8O4T0dPw9xqzTy4SQjsOe+DAm4nD8w0KEkI7El/B2FPR6Dp+LMfuPRQghbcO2aLEEIaTd+FeM2SzLbk28SaMeTNbkWjW3q3EA2tRti43wb8tRjFk1hPQErIfX67UBfhJC+r4Ys7ss3F24DgdjD1wdQvp93BZj1hVCOh574GPYBVPwK9xvIUJIR+Bz+Cha9erEt3C1xdsIP8MIS9aJ43GtZbcD1tfoefxJk2vV3CKmYm11a2JP/NtyFmP2UAjpJ3ARJum1Nc4LIT0gxuwhLxNjNiOE9BP4BY7BPtgLvwohvRtl7IQp+DbOjTF72EKEkLbho/gU2vXqxs/x/RizLosRY/bvENKAgiXrwXTLKIS0iIMwQqM7cb8mV9TEYsyewHUaFVAOIR2rCcSY3Yjj8Yy6bfDbENLNLUSMWU+M2a34APbFDTgU38M6+DbeHGP2xRizhy1ECOkIfAbfwEi9enAFTooxm2MJQkhbMBLtaEc72tGOdrSjHe1oR9Gy2xi7a9SD38aY9WhyrZpfBWWMUrct3ojJmsOv0YYfoYQCtkIlhPR9uC3GrOZlYsx6cFMI6V+xC9bDH2PMplqMENJROBnHo1WvGv6Mj8aYPW/pbIZL0GbJ5uBY3ITHMccCIaQWWAefxEx8L8bseXX7YqJGT+Fqg0BBkwshHYsbsY1GFRwSY9atCYSQtuIjOBXj1E3BR3FVjFm3VyGE1AIr46c4AAW9argZh8WYPWIxQki/h7YYs2MsEEK6Fdos2XzcF2PWGUI6AiMxAxNxFVbGKPwPPhhj9lwI6WrIsLlGv4gxO8Ig0KLJ5Xk+L0k6VsZbUFC3NmKe549rAnme9yRJx9/xAPbESL3G4gALJEnHLUnS0ZXnuWUVQlrAtvgdUhT0quEqHBxjNsUSJEnH29CS5/nVIaQWqGIqpmIqpmIqpmIqpmIqnsX8PM9reZ53J0lHJybiSvRgD/wGx2GrJOmYjHfjSLSom43P5Xn+kEGg1eBwKY7BJHUlHBFC+tcYs5omEGNWCyH9FTrxa7Tq1YqT8WYcHUJ6T4xZzVIIIbXASByDk7GSRpNxeIzZdMtuC/wQLZasC1/Fn0NILTARV6KAd8SYTcVTIaSfx3eR4ONo0+g23GiQaDUIxJg9HEL6K3xSozLOxm2aRIxZLYT0dszFWI3ehL/itBDS02LMZlmMENICdsZp2MXC/TnGbLpX5l78GCMs2VzcrtdEXIkC9o4xm2qBENLV8HHcihRbatSNn8SYdRokWg0eZ+EgTFS3Cj4bQnpQjFm35rEFxlq4lXAyPhBCehrOjTGb4SVCSAvYAZ/BPmixaFuGkLbEmHVbRjFmXbjUMgghnYQr0I13xJhNtUAI6eq4ClV8AlehTaN/4PcGkVaDx324GCdotDfeit9rAiGkRbxb3WycgC9iorpJ+C6OCyE9DxfhCbwVR2EvtGtUw6UoYW+99sAEPK2fhZCuh19jHt4dYzbVAiGkq+MqVHEQjsHGGnXjBzFm0w0iRYNEjFkN5+AJjcbgCyGkYzSH9bGfuutxFnbCLzBPXREJvoQ78Bguw7vQrtF/cBDeh1+gW6+1sb9+FkLagcvQiQNizJ6yQAjp6rgKVRyECfgYWjS6GZcbZIoGl/twjr52wDGWsxDSFhyN1fWq4doYs1qM2eP4IPbAZHSpK2AsVkWbRo/hC9g5xqwSYzYf12G+uqNDSNe0dJ7BtiGkLZZSCOn6uBjzcEiM2eMWCCFdC5djOg7CDJyCNTV6Ad+NMZtukGk1iMSY9YSQnoV9sbW6FpwYQjo5xuxuy88uOELdPNztRTFm83FjCOnfsRuOwR5YSaMe3IuLcD6eijHr8aIYs6khpE9ifb02x9EhpF+NMeuyeJfhWPwmhPQOi/YMfoHZ+Cq6cHiM2aMWCCFdG5dgNg7E8zgQ70JBoyvxR4NQi0EmSTpmYQbehRZ1o7BJknRclud5lwESQjoqSTp2T5KOk/B5rKZuOr6U5/lsL5HneVee5w8lSccVuBxPox3P43p8EV/GH2PMZuR5XvMySdKxA7bSq4AdEZKko5AkHU/neT7LQiRJx3OI2AwTMQETMAETMAETMBb/g5H4JM6IMbvJAiGkk3AhOnFIjNlzSdIxCb/EahpNwdExZo8bhAoGoRDSMfgZDtaoG1+IMfu6fhZCuh72xvuwFcZq1IPvxZgdbwlCSAsYhxGYHmM2zxKEkO6KyRin0Xw8gitxGW6PMZvnFQoh3RKX42Dcho3wY8zHYTFm00JI2/BL7I+Cuh58EV+PMesxCBUMUiGkW+AarK3RTOyLLMbMaymEdBR2wEHYG+uhRV9duBwfiTF7Tj8IIS3gY/gCVrdwz+Pv+DlijNkUyyiEdB+ci99hA0zCPXh/jNnTIaQWOA7fQYtGGcoxZs8apFoMUknS8Qxm4q1oUdeOXXB5nuczvAZCSNdLko4D8Q2cgDdgZRTV9eAx/BFfw7dizGboJ3meS5KOv+NPmI3RWAUt6kZhA+yLdyZJx6Qk6ZieJB3P5HnebSkkScfG2AKzcRN+ih/FmE0LIbVAwFkYqdFz+FCM2f0GsYJBLIR0FH6C9+lrMg6JMZvuFQghHYXNcST2xvoo6GsW7sBluAYPx5h1G2AhpBOwI96LgLVQ0Nfz+AvOw42YEmNmUUJIWzAqxmyWlwkh3QB/wAYadeHT+F6MWbdBrGCQCyFNcAW20teP8N8xZnMshRDSAiYi4AjsjJX01YPHMBmX4ZYYsxc0gRDSFmyEfXEANscoffXgHlyBX+I/MWbzLKUQ0jVwGd6krwo+GGM2wyBXMASEkL4Rv0NJox58Ed+KMZtvEUJIR2MzvBdvxyYWbhbuwoX4PR6NMatpUiGk4/BGHIaANVDQ1/O4DhfhJjwbY1azCCGkK+PHOEhf/8Q+MWa5IaBgCAghLeJI/BgjNOrEZ3FmjNk8LwohLWI17IHD8EaM0VcPpuIaXISbYszmGkRCSIvYCPvhfdgA7frqwT/xW1yGh2LMOr1ECGkJZ+AIfU3DvjFmfzFEFAwRIaRtOBXHo6hRJz6LM9GCDXAo3oONUNTXXDyAs/E7PBJjVjPIhZCOxZ44HAErW7jp+CMuwvWYjrH4Co7T11wcictizGqGiIIhJIR0JH6Og1HQqBPnYyJSjNFXD6q4Fufj2hizeYaoENItcBAOw0SM0Fc3/onf4nUoo6hRFz6NM2LMegwhBUNMCOk4XIK9UbB05uNhXIBfxpg9ZAUSQjoW78QHsTNGo2Dp9OBUfCnGrMcQUzAEhZCOwZ+wi0WrYS4y/AyTY8zmWsGFkG6FI/FeTEDRovXgwhizIwxRBUNUCOnquBrbW7ipOCjGLBrWRwjpSvgcTkRRXzVcgA/EmHUZoloMUXmez06Sjl/i9dgQBY3GYOck6bgjz/PHDPs/IaRFHICTMEZfXfghjo0x6zKEtRjC8jyfnyQdV2JDbIqiRhOwf5J0zE2SjtvzPO+xggshXQXfwskYp695+BpOijHrNsS1GOLyPO9Mko6rMA7bokWjkXgLtkuSjtvyPH/WCiqEdDdchH3Qpq/p+C/8IMasxwqgYAURQtqGD+FUjNdXDU/iFJwTYzbfCiKEdFWcgGMw3sI9hKNxbYxZjxVEwQokhLQFe+E0bGbh5uF6fCXG7CZDWAhpCw7E57Epivqq4c/4OO6NMatZgRSsYEJIC9gQp2I/tFq4Ki7F6bgvxsxQEUJawG74HFKMtHCz8WN8M8bsGSugghVUCOlKeD9OwuoWbQrOxXn4T4xZzSAVQlrEjvgvvA0li3Y3voSrYszmWUEVrMBCSFuwA76APdFm0R7HFbgAd8SYzTNIhJCOwRvxEeyOlS3abFyIb+CRGLOaFVjBMCGk43AwTsDGFu95/AUX4AY8FWNW02RCSFuwPvbEwdgOoy1aD/6Kr+PaGLO5hikY9r9CSAvowMdxOFa1eF14ENfiStyJqTFmPZaTENJWrIsd8W7sgnVQsHgP4we4MMZsmmH/p2BYgxDSIrbFx/AurGrJ5uN+3I4/4e+YgmqMWU0/CSFtxapYBzthT2yLdVC0ZE/hIvwIeYxZzbAGBcMWKoS0gG3xCbwdq1p603Ef7sXtuBNPYBZmYU6MWZelFEI6AqMxFmOxIbbG9tgQG2CUpVPDY7gE5+HeGLOaYQtVMGyJQki3xSEoY220WnYz8RSm4Dk8g2cwHc9pVMQaGI01sSpWxZpYEyMtuzl4ABfi0hizRwxbooJhSy2EdC28HYfi9RiPoubVjWm4CRfiuhizWYYttYJhyyyEtIjN8Fbsh60wBq2Wv/mo4m/4HX4fY/a4Ya9IwbBXJYS0iA68EW/BG7AG2tCq/83HPDyI63Ejbogxe9qwV61g2GsqhLQFm2IrbIOtsQVWQxEFFFGw9HpQQw96kONu3Ik7cQcejzGrGfaaKhg2IEJIx2MDrIsJWB2rYGUL14OpmI0nMQWP4tEYs1mGDYiCYctNCKkliTEzbNiwYcOGDRs2bNiwYcOGDRs2bNiwYcOGDRs2bNiwYcMGUMFLVCtJOzbB7tgU62AeHscduA0PlMp5p4WoVpJPYIS6+0vl/AoLVCvJajgYI716M3FpqZw/X60k6+FAr8wzuLhUzudaStVKUsA62Ak7IUEbnsQjuBH3lsr5c16mWkk2w9tRVPfzUjl/1hJUK0krPoBx6u4olfNrvUS1khyONS1cDdMwCw/hGTxVKuddFqJaSd6InfXqwf2IpXI+0zKoVpIWbIU3ol2vKs5HF96JTbwyd+E2HI5WvbpwM24tlfOapVCtJOOxOzZR93irF1UryUb4At6JlS3cc8iqleQ7uLVUzrs1OgVj1f0aV+i1Fr6Mlb16j+PPeB4b4ltemX/hCsy1FKqVZDyOxgfRgaK+OnFHtZKchUqpnM9S93p8Ey3qrsSzlqwNJ2GSujNxrUYfxw6WrAc5bqlWkvMRS+W8U6OV8FW069WF03GiZbM9rsAa6n5ZKudnVytJKw5D2StzDm7Ennibujl4V7WSXFcq5xanWknacSo+iqJePfhA0QLVSrIbrsbhWNmirYL9MRkftQKpVpISzsXXsAGKFq4dO+MsnFOtJKM1pyLWxyG4AqdXK8nKGv0Fd6lrxSHVSrKWpVStJAUciTXUzcL5XiOlcv4CTsZsdaNwClayZLvgCBTV/Q8uKlYryWo4Bxtp1InpmIH5Go3HBprXfHSiE53oRCc60YlOdGKepVCtJEV8A/uhqK4LMzEdc1BT14rtMcLy1YPpmI4ZmIMejdpxNL5erSQjvahUzp/HbzRaE2+z9CbhrRrdixstWg2d6EQnOtGJTnSiE53oxHy9bsc5Gu2MA6qVpGARqpVkDP4bY9TNxmdL5Xx+K47Fxuq6EfETPIwWbIL3YxeMxDP4oWXzOD6Jdn214HhsqC7Hd9Gpr9l42sLVcCb+bMlmYqYl2xpHoKDuUZyGW9CF1fBu7I9VMR+nlsp51fKV40C9ClgZ6+JAvBmt6o7Cr3CtustxHNbQqxWHVyvJeaVy3mPJUqyrroZflMr5HIv2HI7BHEv2qAVK5byrWklOxXuwjrpP43d41sK9FXuqq+GiUjm/yQKtOBQFdVfjsFI5n6nub9VKcgn2xmG4plTOH7IMSuX8OZxvIaqVZAQOxYbqpuHcUjmfZdndUSrnV3rtHIiR6qZg31I5/4dG11QryRn4CNrxS8vf3FI5v01f51QrySn4LIp6tWH/aiW5rlTOa3r9B9fhUHXbY2vcYTGqlaQVh6NF3eO42uLNwTWlcj7DspmKL+FnKOi1CY6qVpLTSuW8x0tUK8kqOAEj1D2Gr3hRK9bV6IJSOZ/pZUrlfD6uxJVWPFtoFHGXhSiV83twnMHhZJSxsbrXYSxmWqBUzrurleR8HIAReo1BGXdYvG2wjUZ/LpXzh/SDUjmvVSvJ+Xg/3qDuOFyCx7yoWkkssC92VteFb5bK+RNeVERBo07DXq5VoxmoGeRK5bwLf9VoFYzS6G+4Q10B761WknaLtx9WVfcCLtCPSuW8G5/EfHUTcUK1khTVrYVPoajuNpzrJYqoafTJaiVZzbCXmqvRvtjV0DBSoyrmalTFxRqthb0tQrWSlFDW6G78Rf/7O85GTd2R2ErdB7GZuln4fKmcz/ESRTyoUcAN1UrywWolWdWw/9/tqKlbA1dXK8kZ1UqyRbWSFA1C1UoyDjtr9CBmeYlSObfA7/CUujYcYdFSrKeuBz8rlfMX9LNSObfAVzBN3XgcX60kI6qVZH0cpdHFuM7LtOKn+C6KehWwKX6Cb1YryXW4ApNL5fw5g8MG1Uqyk0Wrlsr5fZbeZTgRY9WNx3/hw/hntZL8FleVyvndBo/TMUldDyqlct6jr8fwBxypbu9qJVm7VM6f0NdhaFf3CK61dEZg+2olmW3R7iuV86pFKJXzp6uV5Ev4IVr02hffxz5YT92j+EapnNe8TCvOxnvwRhTUtWAVlLEfplYrSQU/LJXzBzWvAj6NEyzan/BuS+9+nIyvo01dAaOwI3bAZ6uV5HqcjhtK5bzb8tdarSQT1Y3E6/B+7IOCuitxvYUolfOuaiU5H+9Fm14jcTBO8xLVSrIRdtPoGuSWzmq4EjWLdgCusXjn4zDsptc4nIxNUdBrPr6DhyxEsVTOX8DBuABzLVwb1sYn8KdqJTlIc2vHGIzBGIzh/2sPXoDsKggDgJ7dffmxcbkJlBcbJt6QMn7aWqn9aCFwH1qKOshvrnSmtQ7+pZXaoJQwAp0xpcVRVKZNLdJqysfQi4mUKmkQ3jUNg4EmgQaGz6TNzUdINTE3my/7e83MMnN9JLvvZQPDQ+45etGLXkx1FII4a+AmLMBPHFkX+nAe7saX8iSc4pV3CtZjPdbjYSS4CBWFh/GZIM72Gtt6rFXowkfyJOzR7H04UWEflgZx1tCeLhyHXvSiF73oRS96UdFCEGcH8HnsUzgXcxUewW1BnDmSbocEcfYcPok/wBJsRcPhujAXi/MknO81JIizQSxGhEVYh0FH9jr8Ka7zyqvgJJyEk3ACpirsxmJcgMz4+rFEszfinV6QJ2EFH0SPwn9hnVfGKtyGhlE96DZqDxYFcbbLGCpeEMTZAazKk/AhzMJv4704E7+CLoWZWJQnYS2IsxGdpYGv4QFj+4kJCOJsBE/mSXgtvoo34xy8G6dhqsIkfDJPwiVBnD2tM+3FH2NFEGdDWgjirJEn4b/jOswyqhsfypPwwSDOGjgdb1Ro4PYgzvZq305chgPG9og2BHE2kifhDTgXb9DsO7jfOCpeJIizQWzF1jwJv4uTcQkWYobC23EKNuo864M4u8fLJIizBnZiNVbnSXgjTsf1eKtCgPfhaWObqT1dmO7obcNfoIKz8GFMNmo6rsKPsEN7tuNufELhQlyJXfgAehU24T5H5wBWBHHW7yUQxNmmPAkTfFZhD/4piLMB4+g2jiDORoI424IbcZ1mFcxVEsRZHsTZ9/BhPKvQhTcoHERDsznaMwMzNDuotf4gzu4K4mwp/hx/r9np+HyehJO0IYizIfwLhhVOwAV5Ep6ICzS7B1u88vZoNoz9WujWhiDOhrECuUI3pij9vCewQbNuhacwpFktT8Ie48iT0CHvcrjHHYUgzgZwPZ7S7OM4O09CbXoMazS7GO/CLynsw7eDOBvxKlXJk3AaerEziLOGsU3FFIUhbPcakCdhF05EHsTZoLFNwjTNcoVn8BzmKrwXb8ITxtaHj2o2gIccvR34LJZhslHT8EX8N57T2j7cit9TmI9+TFJ4EI97FevGTViJq/IkPMER5EnYg/djmsIePOG14Q+xEt/Ik/DNeRJ2O7Jfx9sUBrHeC4I4G8JSNBROxpfyJAzzJOzyInkSzsC1mK/ZKmSOUhBnDrkX/6zZW3F1noRTtBDEmUOWY4dCH85XaODOIM72eRWr4FL04DS8J0/Cr2AN9hh1PC7CFQoNLA/i7IDONDNPwtlaG8JPgzgbMYY8CV+PL+P1eBtquDlPwn/FDgxgGn4Hf40+he24X7PF+CPMUTgXy3BznoQPYB8m4zdwOc7SbAB/E8TZ8yYgiLORPAmvwe9jnsJHUc+TcHkQZw3j24Gl+DOF4xQyrDAxPfjlPAlfp7X9QZzt8jKpoEdhPuZjC7ZgBPMwW7P/w9d0pi78Fa7S2lO4ELscQZ6EDvkMZinMwSJcgf9BP2bhVExSGMQXgzjb7ecEcbYtT8KrsRh9CqfhH3AQuzENfQ43jJvwQ8cgiLMdeRJ+Dksx2aip+ALWYrNxBHE2nCfhEnwKPQ73b9huYqr4IRpaux1XeJl0Y6fDzcEZOBOzNcvxuSDOntS5jkcVVVRRRRVVVFFFFSeiy/huxWNoaDYDv4Wz8RZMUhjCt3CLIwji7Hb8JXKHm4oq+hxuADdjYRBnw45REGfLsQwNhbdgYZ6EU7T2KH7kcPtxaxBnIyamGyehiiqqqKKKKqqooorjvYy6UcM92I+GsQ3jSVwSxNltXgOCOBPE2eOo4Qb8FCPG1sDP8AVcHsTZQWMI4uzr+AAewaDxDWMLPoVPB3E25KWzAJs1+xAuzJOwy/iGsMThVuFJvwAqQZxtwPvzJPw1XIyz8CbMNOpZPIbluDuIs93Gdh+mKqzXnhGswR6FZzCstZ2418RsxqAWgjjLsTBPwhtxMc7GaZiNbuzF40jxrSDOMm0I4uy+PAlX4d04H7+LEFMwjB9jLVZgWRBne7T2EHYobDGOIM6ey5PwPCzALIUz8H30G0MQZ/Ik/DbOQ0Xh74I42689DTyK6SZmg/ZsxL0K+9CvVCqVSqVSqVQqlUqlUqlUKpVKpVKpVCqVXqxLacKiqNaHeQr92Jqm9QFtiKLaqZhu1BC2pmk9dxSiqDYJczETB7E1Tes7lV4SXUoTFkW1c3Cnwm6swjVpWt+shSiq3Y0zjRrERtyI76RpvaGFKKqFuB5nYiYOYiNuwHfTtD6sdEwqSseiggBfwf04C5fjeXxMa9PRj4UYwfW4AWuxyTiiqNaLW/GbuAMrEeJj+EccwPeVjklFB4ii2nQcpzPlaVofML4NaVr/XhTVHsG5eIf27cUDaVrfHkW10/EnmIVNxncR3oGvY0Ga1gcdEkW1tbgLC6Ko9oM0rQ84giiq9aJXZ9qdpvXndYCKznAZPqgzfQQPG19vFNVOwK/iZKzRvsmYG0W1eXgntmGr1t6OCm5L0/qgwmo8jXmYjU2O7FJ8Qmf6NFIdoKIzbMM6naeBPVq7Eh/HHOT4svadgh9gGjbisjStb9Naj1H9mg1hAD3oMbZnsU5nynWIig6QpvU7cIdXrw14FFvxH2la36R9m3ElrkUftmvPj9HAGVFUeypN6w2j5mA2foYdxpCm9WVYpjSuitJL4a40rX/TxBzAalyJO/G3UVS7JE3r+4xvGa7A1dgURbU1CHA95uK6NK3nSsekW6lTrMQtOAeXRlHNeNK0/gwuQx9WYhv+FxfjFnxV6ZhVlI7FM7gG60zMNzEde9O0PhJFtUXIMIRJGDSONK0nUVRbjfNxKnbhP/FgmtaHlI7Z/wPBOFON4FAibAAAAABJRU5ErkJggg==';

      let reportingLogo = '';

      if (companyLogo === '') {
        reportingLogo = defaultLogo;
      }

      else {
        reportingLogo = companyLogo;
      }

    let date = new Date();
    let options = {
      html:
    `
    <style>
    table.minimalistBlack {
      border: 3px solid black;
      width: 100%;
      text-align: center;
      border-collapse: collapse;
    }
    table.minimalistBlack td, table.minimalistBlack th {
      border: 1px solid black;
      padding: 5px 4px;
    }
    table.minimalistBlack tbody td {
      font-size: 15px;
    }
    table.minimalistBlack thead {
      
      border-bottom: 3px solid black;
    }
    table.minimalistBlack thead th {
      font-size: 18px;
      font-weight: bold;
      color: black;
      text-align: center;
    }
    table.minimalistBlack tfoot td {
      font-size: 13px;
    }

    .logo {
      display: block;
      margin-left: auto;
      margin-right: auto;
    }

    .sub-heading {
      text-align: center;
    }
        </style>
              <img src=${`data:image/jpg;base64,${reportingLogo}`} class="logo" />
              <h4 class="sub-heading">Report Created By ${createdBy} On ${date}</h4>
              <table class="minimalistBlack">
              <thead>
              <tr>
              <th>${projectNameLabel}</th>
              <th>${projectNoteLabel}</th>
              </tr>
              </thead>
              <tbody>
                <td>${projectName.replace(/_/g, ' ')}</td>
                <td>${projectNote}</td>
              </tbody>
            </table>
              <br>
              <table class="minimalistBlack">
              <thead>
              <tr>
              <th>${field01Label}</th>
              <th>${field02Label}</th>
              <th>Image</th>
              </tr>
              </thead>
              <tbody>
                ${reportOutput}
              </tbody>
            </table>
    `,
      fileName: this.state.projectName + '_report',
      // directory: 'Documents',
    };
 
    let file = await RNHTMLtoPDF.convert(options);
    console.log("file://" + file.filePath);
    this.setState({filePath: "file://" + file.filePath});
    Alert.alert(
      'Awesome!',
      'Report created successfully. Do you want to share this now?',
      [
        {text: 'Yes', onPress: () => this.shareFile()},
        {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
      ],
      {cancelable: false},
    );
  }

    shareFile = () => {
      const {filePath} = this.state;
      if (filePath !== '') {
        Share.open({
          subject: this.state.sendEmailSubject,
          message: this.state.sendEmailBody,
          url: this.state.filePath,
        });
        console.log('Share Done');
      }

      else {
        Alert.alert(
          'Wait',
          'You need to save the report first before Email. Please save it.',
          [
            {text: 'OK'},
          ],
          { cancelable: false }
        )
      }
      
    }

  render() {
    const { navigation,route } = this.props;
    return (

      // Add Data Modal

      <Container>
        <Modal
        animationType="slide"
        visible={this.state.modalVisible}
        >
           <Content contentContainerStyle={styles.modal}>
        <Text
        style={{
            lineHeight: 30,
            marginBottom: 10,
            fontSize: 17
          }}
        >{this.state.field01Label}</Text>
          <Item regular
          style={{
            marginBottom: 30,
          }}
          >
            <Input
            placeholder="Enter Name"
            onChangeText={data => this.setState({ location: data })}
            style={styles.textInputStyle}
            underlineColorAndroid='transparent'
            />
          </Item>
          <Text
        style={{
            lineHeight: 30,
            marginBottom: 10,
            fontSize: 17
          }}
        >{this.state.field02Label}</Text>
          <Item regular
          style={{
            marginBottom: 30,
          }}
          >
            <Input
            placeholder="Enter Name"
            onChangeText={data => this.setState({ note: data })}
            style={styles.textInputStyle}
            underlineColorAndroid='transparent'
            />
          </Item>
          <Button block
          style={{
            marginBottom: 10,
            backgroundColor: '#2B2D2F',
          }}
          onPress={this.chooseImage}
          >
            <Text>CHOOSE IMAGE</Text>
          </Button>
          <Button block
          style={{
            marginBottom: 10,
            backgroundColor: '#2B2D2F',
          }}
          onPress={this.captureImage}
          >
            <Text>CAPTURE IMAGE</Text>
          </Button>
          <Button block
          style={{
            marginBottom: 10,
            backgroundColor: '#87C540',
          }}
          onPress={this.saveData.bind(this)}
          >
            <Text>SAVE</Text>
          </Button>
          <Button block danger
          style={{
            marginBottom: 10,
          }}
          onPress={() => {this.setModalVisible(false);}}
          >
            <Text>CANCEL</Text>
          </Button>
        </Content>
        </Modal>


        {/* Edit Data Modal */}

        <Modal
        animationType="slide"
        visible={this.state.editmodalVisible}
        >
           <Content contentContainerStyle={styles.modal}>
        <Text
        style={{
            lineHeight: 30,
            marginBottom: 10,
            fontSize: 17
          }}
        >{this.state.field01Label}</Text>
          <Item regular
          style={{
            marginBottom: 30,
          }}
          >
            <Input
            placeholder="Enter Name"
            onChangeText={data => this.setState({ location: data })}
            style={styles.textInputStyle}
            underlineColorAndroid='transparent'
            />
          </Item>
          <Text
        style={{
            lineHeight: 30,
            marginBottom: 10,
            fontSize: 17
          }}
        >{this.state.field02Label}</Text>
          <Item regular
          style={{
            marginBottom: 30,
          }}
          >
            <Input
            placeholder="Enter Name"
            onChangeText={data => this.setState({ note: data })}
            style={styles.textInputStyle}
            underlineColorAndroid='transparent'
            />
          </Item>
          <Button block
          style={{
            marginBottom: 10,
            backgroundColor: '#2B2D2F',
          }}
          onPress={this.chooseImage}
          >
            <Text>CHOOSE IMAGE</Text>
          </Button>
          <Button block
          style={{
            marginBottom: 10,
            backgroundColor: '#2B2D2F',
          }}
          onPress={this.captureImage}
          >
            <Text>CAPTURE IMAGE</Text>
          </Button>
          <Button block
          style={{
            marginBottom: 10,
            backgroundColor: '#87C540',
          }}
          onPress={this.editData.bind(this)}
          >
            <Text>UPDATE DATA</Text>
          </Button>
          <Button block danger
          style={{
            marginBottom: 10,
          }}
          onPress={() => {this.editModalVisible(false);}}
          >
            <Text>CANCEL INPUT</Text>
          </Button>
        </Content>
        </Modal>
        
        {/* <Text>Project Name is {this.state.projectName} </Text> */}
          <Content>
        <FlatList
          data={this.state.arrayHolder}
          // width='100%'
          extraData={this.state.arrayHolder}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={this.ListEmpty}
          renderItem={({ item }) => 
          <ListItem
          title={item.location}
          titleStyle={{ fontWeight: 'bold', fontSize: 18 }}
          subtitle={item.note}
          subtitleStyle={{ fontSize: 15 }}
          leftAvatar={{ rounded: true, size: 'large', source: { uri: item.image } }}
          bottomDivider
          chevron
          style={{
            marginTop: 10,
          }}
          onPress={() => {
            var allitems = Object.values(item);
            this.setState({dataid: allitems[4]});
            // dataid = allitems[4]
            // console.log(allitems[4]);
            this.editModalVisible(true);
          }}
          onLongPress={ () =>
            Alert.alert(
              'Are You Sure!',
              'Do you want to delete this data?',
              [
                {text: 'Yes', onPress: () => {
                  const {projectName} = this.state;
                  db.transaction(tx => {
                    console.log(item.id);
                    tx.executeSql('DELETE FROM ' + projectName + ' WHERE Id='+ item.id, [], (tx, results) => {
                      this.readData();
                    });
                  });
        
                }},
                {text: 'No'},
              ],
              {cancelable: false},
            )
          
          }
          />
        }
        />
        </Content>
        

        <Footer>
          <FooterTab
          style={{
            backgroundColor: '#87C540',
          }}
          >
            <Button vertical
            onPress={this.createPDF}
            >
              <Icon name="download" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Save Report</Text>
            </Button>
            <Button vertical
            onPress={() => {this.setModalVisible(true);}}
            >
              <Icon name="folder" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Add Location</Text>
            </Button>
            <Button vertical
            onPress={this.shareFile}
            >
              <Icon name="mail" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Email Report</Text>
            </Button>
          </FooterTab>
        </Footer>

      </Container>
    );
  }
}

const styles = StyleSheet.create({
    content: {
      justifyContent: 'center',
      alignItems: 'center',
      flex:1,
      paddingLeft: 20,
      paddingRight: 20
    },

    modal: {
      justifyContent: 'center', //Centered vertically
      flex:1,
      paddingLeft: 50,
      paddingRight: 50,
      
    },

    addNewButton:{
 
      position: 'absolute',
      width: 50,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
      right: 30,
      bottom: 30,
    },

    createPDFButton:{
 
      position: 'absolute',
      width: 50,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
      left: 30,
      bottom: 30,
    },
   
    FloatingButton: {
      resizeMode: 'contain',
      width: 50,
      height: 50,
    }
  });