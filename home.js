import React, { Component } from 'react';
import { StyleSheet} from 'react-native';
import { Container, Content, Text, Button, Footer, FooterTab, Icon} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import RNBootSplash from "react-native-bootsplash";
export default class Home extends Component {

  componentDidMount() {
    RNBootSplash.hide({ duration: 300 });
  }

  render() {
    const { navigation} = this.props;
    return (
      <Container>
            <Container style={styles.container}>
            <Content contentContainerStyle={styles.content} >
                <Text
                style={{
                  textAlign: 'center',
                  lineHeight: 30,
                  marginBottom: 20
                }}
                >
                  No forms created yet. Please tap on ‘ Create New Project’ to create a new one
                  </Text>
                  <Button primary
                  style={{
                    backgroundColor: '#2B2D2F',
                    marginBottom: 10
                  }}
                  onPress={() => navigation.navigate("Input Data")}
                  >
            <Text>Create New Project</Text>
          </Button>
            </Content>
        </Container>
        <Footer>
          <FooterTab
          style={{
            backgroundColor: '#87C540',
          }}
          >
            <Button vertical
            onPress={() => navigation.navigate("Project List")}
            >
              <Icon name="list"
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Project</Text>
            </Button>
            <Button vertical
            onPress={() => navigation.navigate("Settings")}
            >
              <Icon name="power" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Settings</Text>
            </Button>
            <Button vertical
            onPress={() => navigation.navigate("About")}
            >
              <Icon name="person" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >About</Text>
            </Button>
          </FooterTab>
        </Footer>

        </Container>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    justifyContent: 'center', //Centered vertically
    alignItems: 'center', // Centered horizontally
    flex:1,
    paddingLeft: 50,
    paddingRight: 50,
    
  }
});