import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './home';
import AppFooter from './footer';
import DataList from './main';
import ProjectList from './project-list';
import Settings from './settings';
import FieldSettings from './field-settings';
import ProjectSettings from './project-settings';
import ReportSettings from './report-settings';
import EmailSettings from './email-settings';
import LoadData from './load-data';
import About from './about';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Site Survey Pro"     
      >
        <Stack.Screen name="Site Survey Pro" component={Home} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />

        <Stack.Screen name="Input Data" component={DataList} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />


<Stack.Screen name="Load Data" component={LoadData} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />

        <Stack.Screen name="Settings" component={Settings} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />


<Stack.Screen name="Project Settings" component={ProjectSettings} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />

<Stack.Screen name="Field Settings" component={FieldSettings} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />

<Stack.Screen name="Report Settings" component={ReportSettings} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />

<Stack.Screen name="Email Settings" component={EmailSettings} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />

<Stack.Screen name="Project List" component={ProjectList} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />


<Stack.Screen name="About" component={About} 
        options={{
          headerStyle: {
            backgroundColor: '#2B2D2F'
          },

          headerTintColor: '#fff',
          headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 18,
          },
        }}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

console.disableYellowBox = true;

export default App;
