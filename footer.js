import React, { Component } from 'react';
import { Container, Content, Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';
export default class AppFooter extends Component {

    constructor(props){
        super(props);
    }

  render() {
    const { navigation, route} = this.props;
    return (
      <Container>
        <Content />
        <Footer>
          <FooterTab>
            <Button vertical>
              <Icon name="home" />
              <Text>Home</Text>
            </Button>
            <Button vertical>
              <Icon name="list" />
              <Text>Project</Text>
            </Button>
            <Button vertical
            onPress={() => navigation.navigate("Input Data")}
            >
              <Icon name="power" />
              <Text>Settings</Text>
            </Button>
            <Button vertical>
              <Icon name="person" />
              <Text>About</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}