import React, { Component } from 'react';
import { StyleSheet, Image} from 'react-native';
import { Container, Content, Text, Button, Footer, FooterTab, Icon} from 'native-base';
export default class About extends Component {

  render() {
    const { navigation} = this.props;
    return (
      <Container>
            <Container style={styles.container}>
            <Content contentContainerStyle={styles.content} >
            <Image
                style={{
                    marginBottom: 30
                }}
                source={require('/Users/anonnaabir/Desktop/SiteSurveyPro/assets/bootsplash_logo.png')}
      />
                  <Text
                style={{
                  textAlign: 'center',
                  marginBottom: 10
                }}
                >
                  Site Survey tool is great for general contractors like plumbers, electricians, security integrators, hvac companies etc. Site Survey Tool can help you with project management, quoting jobs, and inspecting jobs. The app allows you to take pictures of different locations at your job site that you can submit into a PDF report with your logo. Give Site Survey Pro a Try today!
                  </Text>
                  {/* <Text
                style={{
                  textAlign: 'center',
                  marginBottom: 10
                }}
                >
                  Created By Asaduzzaman Abir (Codergem.com)
                  </Text> */}
            </Content>
        </Container>
        <Footer>
          <FooterTab
          style={{
            backgroundColor: '#87C540',
          }}
          >

            <Button vertical
            onPress={() => navigation.navigate("Site Survey Pro")}
            >
              <Icon name="home"
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >HOME</Text>
            </Button>
            <Button vertical
            onPress={() => navigation.navigate("Project List")}
            >
              <Icon name="list"
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Project</Text>
            </Button>
            <Button vertical
            onPress={() => navigation.navigate("Settings")}
            >
              <Icon name="power" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Settings</Text>
            </Button>
          </FooterTab>
        </Footer>

        </Container>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    justifyContent: 'center', //Centered vertically
    alignItems: 'center', // Centered horizontally
    flex:1,
    paddingLeft: 50,
    paddingRight: 50,
    
  }
});