import React, { Component } from 'react';
import { StyleSheet, Modal, TouchableOpacity, View, Alert} from 'react-native';
import { Container, Content, Footer, FooterTab, Text, Button, List, ListItem, Left, Right, Icon, Form, Item, Label, Input} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-community/async-storage';

export default class ReportSettings extends Component {

    constructor(props){
        super(props);
        this.state = {
           companyLogo: '',
           createdBy: '',
        }
    }

    chooseImage = () => {
      ImagePicker.openPicker({
        width: 150,
        height: 75,
        cropping: true,
        includeBase64: true,
      }).then(image => {
        this.imageData(image);
      });
    }
  
    imageData = (image) => {
      this.setState({companyLogo: image.data});
    }

      saveSettings = async () => {
    try {
      console.log('Write Start');
      const {companyLogo} = this.state;
      const {createdBy} = this.state;
      await AsyncStorage.setItem('CompanyLogo', companyLogo);
      await AsyncStorage.setItem('CreatedBy', createdBy);
      console.log('Write Done');
      Alert.alert(
        'Done',
        'Settings Saved Successfully',
        [
          {
            text: 'OK',
            onPress: () => {this.gotoallSettings()}
          },
        ],
        { cancelable: false }
      );
    } 
    catch (error) {
      console.log('Data Save Err');
    }
  };

    
    gotoallSettings = () => {
        const { navigation} = this.props;
        navigation.navigate('Settings');
    }


    //Render Part

    render() {
        const { navigation} = this.props;
        return(
            <Container>
                <Content contentContainerStyle={styles.content}>

                    <Text style={styles.inputboxTitle}>Report Created By (Default: Site Survey Pro)</Text>
                        <Item regular style={styles.item} >
                            <Input
                            onChangeText={(text) => this.setState({createdBy: text})}
                            placeholder="Example: Garrett Thomas"
                            />
                        </Item>

                    <Text style={styles.inputboxTitle}>Company Logo</Text>
                        <Item regular style={styles.item}>
                        <Button block
                        style={{
                          backgroundColor: '#87C540',
                        }}
                        onPress={this.chooseImage}>
                          <Text>CHOOSE LOGO</Text>
                          </Button>
                        </Item>

                    <Button
                      style={styles.submitButton}
                      onPress={this.saveSettings}>
                        <Text>SAVE SETTINGS</Text>
                    </Button>
                    
                </Content>
                
                <Footer>
                    <FooterTab
                    style={{
                      backgroundColor: '#87C540',
                    }}
                    >

                        <Button vertical
                        onPress={() => navigation.navigate("Site Survey Pro")}>
                          <Icon name="home" 
                          style={{
                            color: '#FFFFFF',
                          }}
                          />
                          <Text
                          style={{
                            color: '#FFFFFF',
                          }}
                          >Home</Text>
                        </Button>
            
                        <Button vertical>
                          <Icon name="list" 
                          style={{
                            color: '#FFFFFF',
                          }}
                          />
                          <Text
                          style={{
                            color: '#FFFFFF',
                          }}
                          >Project</Text>
                        </Button>
            
                        <Button vertical
                        onPress={() => navigation.navigate("Settings")}>
                          <Icon name="power" 
                          style={{
                            color: '#FFFFFF',
                          }}
                          />
                          <Text
                          style={{
                            color: '#FFFFFF',
                          }}
                          >Settings</Text>
                        </Button>
                        
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}


// Style Part

const styles = StyleSheet.create({
    content: {
      //justifyContent: 'center',
      //alignItems: 'center',
      flex:1,
    //   paddingLeft: 20,
    //   paddingRight: 20,
      marginTop: 30,
    },

    inputboxTitle: {
        lineHeight: 30,
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        fontSize: 17
    },

    item: {
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20
        // paddingLeft: 20,
        // paddingRight: 20,
    },

    submitButton: {
        textAlign: "center",
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#2B2D2F',

    }
  });