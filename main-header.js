import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Alert, TouchableOpacity, TextInput, Button, Image } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Icon, Content, Text } from 'native-base';
export default class MainHeader extends Component {
  render() {
    return (
            <Header style={styles.header}>
                <Left style={{flex:1}} />
                <Body style={styles.body}>
                    <Title
                    style={{
                      color: '#FFFFFF'
                    }}
                    >Site Survey Pro</Title>
                </Body>
                <Right style={{flex:1}} />
            </Header>
            
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#3700B3',
    justifyContent: 'center', //Centered vertically
    alignItems: 'center', // Centered horizontally
  },

  body: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});