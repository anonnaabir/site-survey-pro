import React, { Component } from 'react';
import { StyleSheet, Modal, TouchableOpacity, View, Alert} from 'react-native';
import { Container, Content, Text, Button, List, ListItem, Left, Right, Icon, Footer, FooterTab} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

export default class Settings extends Component {

  //   constructor(props){
  //       super(props);
  //       this.state = {
  //           projectNameTitle: 'Dugdugi',
  //           projectNoteTitle: 'Ayan',
  //           createProjectButtonTitle: 'Anonna',
  //           field1Title: 'Name',
  //           field2Title: 'Location',
  //           emailSubject: 'This is subject',
  //           emailMessage: 'This is messege',
  //       }
  //   }

  //     saveSettings = async () => {
  //   try {
  //     const {arname} = this.state;
  //     await AsyncStorage.setItem('amarname', arname);
  //   //   const { navigation} = this.props;
  //   //     navigation.navigate('Input Data', {passname: this.state.arname,});
  //     console.log(arname);
  //   } 
  //   catch (error) {
  //     console.log('Data Save Err');
  //   }
  // };
    
  //   setSettings = () => {
  //       const { navigation} = this.props;
  //       navigation.navigate('Input Data', {passname: this.state.arname,});
  //       console.log("Data Passed Success");
  //       //console.log("Name is " + this.state.arname);
  //   }

  //   fieldSettings = () => {
  //       const {projectNameTitle} = this.state.projectNameTitle;
  //       if (projectNameTitle) {
  //           return(
  //               <Container>
  //               <Modal
  //               visible={true}
  //               >
  //                   <Text>Alhamdulillah</Text>
  //               </Modal>
  //               </Container>
  //       );
  //           }
            
  //           else {
  //               Alert.alert(
  //                   'Awesome!',
  //                   'Report created successfully. Do you want to share this now?',
  //                   [
  //                     {text: 'Yes', onPress: () => this.shareFile()},
  //                     {text: 'Yes', onPress: () => this.shareFile()},
  //                     {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
  //                   ],
  //                   {cancelable: false},
  //                 );
  //           }
  //   }

    render() {
        const { navigation} = this.props;
        return(
            <Container>
                <Content>
                    <List>

                    <ListItem 
                        button
                        onPress={() => navigation.navigate("Project Settings")}>
                          <Left>
                            <Text>Project Settings</Text>
                          </Left>
                          <Right>
                            <Icon name="arrow-forward" />
                          </Right>
                        </ListItem>

                        <ListItem 
                        button
                        onPress={() => navigation.navigate("Field Settings")}>
                          <Left>
                            <Text>Fields Settings</Text>
                          </Left>
                          <Right>
                            <Icon name="arrow-forward" />
                          </Right>
                        </ListItem>
            
                        <ListItem
                        button
                        onPress={() => navigation.navigate("Report Settings")}>
                          <Left>
                            <Text>Report Settings</Text>
                          </Left>
                          <Right>
                            <Icon name="arrow-forward" />
                          </Right>
                        </ListItem>

                        <ListItem
                        button
                        onPress={() => navigation.navigate("Email Settings")}>
                          <Left>
                            <Text>Email Settings</Text>
                          </Left>
                          <Right>
                            <Icon name="arrow-forward" />
                          </Right>
                        </ListItem>

                    </List>
                </Content>
                
                <Footer>
                  <FooterTab
                  style={{
                    backgroundColor: '#87C540',
                  }}
                  >
                      <Button vertical
                            onPress={() => navigation.navigate("Site Survey Pro")} >
                        <Icon name="home" 
                        style={{
                          color: '#FFFFFF',
                        }}
                        />
                        <Text
                        style={{
                          color: '#FFFFFF',
                        }}
                        >Home</Text>
                    </Button>

                      <Button vertical
                            onPress={() => navigation.navigate("Project List")}>
                        <Icon name="list" 
                        style={{
                          color: '#FFFFFF',
                        }}
                        />
                        <Text
                        style={{
                          color: '#FFFFFF',
                        }}
                        >Project</Text>
                      </Button>
            
                      <Button vertical
                            onPress={() => navigation.navigate("About")}>
                        <Icon name="person" 
                        style={{
                          color: '#FFFFFF',
                        }}
                        />
                        <Text
                        style={{
                          color: '#FFFFFF',
                        }}
                        >About</Text>
                      </Button>
                  </FooterTab>
              </Footer>

            </Container>
        )
    }
}