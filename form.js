import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Alert, TouchableOpacity, TextInput, Button, Image } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import RNHTMLtoPDF from 'react-native-html-to-pdf';

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.array = [{
      title: 'Garrett', location: 'USA', image: '', imageData: ''
    },
    ],

      this.state = {
        arrayHolder: [],
        PlaceName: '', LocationName: '', myImage: '', myImageData: '',
        filePath: '',
        name: 'Ayan'
      }
      
  }

  componentDidMount() {
    this.setState({ arrayHolder: [...this.array] })
  }


    addDatas = () => {
    this.array.push({
      title : this.state.PlaceName,
      location : this.state.LocationName,
      image : this.state.myImage,
      imageData : this.state.myImageData,
    });

    this.setState({ arrayHolder: [...this.array] })
  }

  chooseImage = () => {
    ImagePicker.openPicker({
      width: 600,
      height: 300,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      this.onSelectedImage(image);
      this.imageData(image);
      console.log(image.data);
    });
  }

  captureImage = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      this.onSelectedImage(image);
      this.imageData(image);
      console.log(image);
    });
  }

  onSelectedImage = (image) => {
    console.log(image.path);
    this.setState({myImage: image.path});
  }

  imageData = (image) => {
    console.log(image.data);
    this.setState({myImageData: image.data});
  }
  
    createPDF = async() => {
      const { arrayHolder } = this.state;
      let reportOutput = arrayHolder.map((i) => {
        return `
        <tr>
        <tdi.title}</td>
        <td>${i.location}</td>
        <td><img src=${`data:image/jpg;base64,${i.imageData}`} /></td>
        </tr>
        `;
     });
      // var i;
      // for (i = 0; i < arrayHolder.length; i++) {
      //   var pdfData = arrayHolder[i].title;
      // }
      const pdfHeader = "<h1>Alhamdulillah. I love Anonna And Ayan</h1>";
      let options = {
        html:
      `${pdfHeader}
      <style>
      body {
        line-height: 1.25;
      }
      
      table {
        border: 1px solid #ccc;
        border-collapse: collapse;
        margin: 0;
        padding: 0;
        width: 100%;
        table-layout: fixed;
      }
      
      table caption {
        font-size: 1.5em;
        margin: .5em 0 .75em;
      }
      
      table tr {
        background-color: #f8f8f8;
        border: 1px solid #ddd;
        padding: .35em;
      }
      
      table th,
      table td {
        padding: .625em;
        text-align: center;
      }
      
      table th {
        font-size: .85em;
        letter-spacing: .1em;
        text-transform: uppercase;
      }
      
        table caption {
          font-size: 1.3em;
        }
        
        table thead {
          border: none;
          clip: rect(0 0 0 0);
          height: 1px;
          margin: -1px;
          overflow: hidden;
          padding: 0;
          position: absolute;
          width: 1px;
        }
        
        table tr {
          border-bottom: 3px solid #ddd;
          display: block;
          margin-bottom: .625em;
        }
        
        table td {
          border-bottom: 1px solid #ddd;
          display: block;
          font-size: .8em;
          text-align: right;
        }
        
        table td::before {
          /*
          * aria-label has no advantage, it won't be read inside a table
          content: attr(aria-label);
          */
          content: attr(data-label);
          float: left;
          font-weight: bold;
          text-transform: uppercase;
        }
        
        table td:last-child {
          border-bottom: 0;
        }
      }
          </style>
                <table>
                <caption>Statement Summary</caption>
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Location</th>
                    <th scope="col">Image</th>
                  </tr>
                </thead>
                <tbody>
                  ${reportOutput}
                </tbody>
              </table>
      `,
        fileName: 'test',
        directory: 'docs',
      };
   
      let file = await RNHTMLtoPDF.convert(options);
      console.log(file.filePath);
      Alert.alert(file.filePath);
      this.setState({filePath:file.filePath});
    }

    testArrayData = () => {
      const { arrayHolder } = this.state;
      let reportOutput = arrayHolder.map((i) => {
        return `
        <tr>
        <td>${i.title}</td>
        <td>${i.location}</td>
        <td><img src=${`data:image/png;base64,${i.imageData}`} /></td>
        </tr>
        `;
     });

     console.log(reportOutput);
      // var i;
      // for (i = 0; i < arrayHolder.length; i++) {
      // var pdfData = "<td>" + arrayHolder[i].title + "</td>" ;
      // }
      // var finalData = pdfData;
      // console.log(finalData);
    }

  
  render() {
    return (
      <View style={styles.MainContainer}>
        <TextInput
          placeholder="Enter Value Here"
          onChangeText={data => this.setState({ PlaceName: data })}
          style={styles.textInputStyle}
          underlineColorAndroid='transparent'
        />
        <TextInput
          placeholder="Enter Value Here"
          onChangeText={data => this.setState({ LocationName: data })}
          style={styles.textInputStyle}
          underlineColorAndroid='transparent'
        />
        <Text>{'Save File Path = ' + this.state.filePath}</Text>        
        <TouchableOpacity onPress={this.chooseImage} sactiveOpacity={0.7} style={styles.button} >
        <Text style={styles.buttonText}> Choose Image </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.captureImage} sactiveOpacity={0.7} style={styles.button} >
        <Text style={styles.buttonText}> Capture Image </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.addDatas} sactiveOpacity={0.7} style={styles.button} >
        <Text style={styles.buttonText}> Add Values To FlatList </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.createPDF} sactiveOpacity={0.7} style={styles.button} >
        <Text style={styles.buttonText}> Make Report</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.testArrayData} sactiveOpacity={0.7} style={styles.button} >
        <Text style={styles.buttonText}> Test Data </Text>
        </TouchableOpacity>
        <FlatList
          data={this.state.arrayHolder}
          width='100%'
          extraData={this.state.arrayHolder}
          keyExtractor={(index) => index.toString()}
          renderItem={({ item }) => 
          <View>
          <Content>
          <List>
            <ListItem avatar>
              <Left>
                <Thumbnail source={{ uri: item.image }} />
              </Left>
              <Body>
                <Text>{item.title}</Text>
                <Text note>{item.location}</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
          </List>
        </Content>
            </View>
        }
        />
      </View>

    );
  }
}

const styles = StyleSheet.create({

  MainContainer: {

    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    margin: 2

  },

  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },

  textInputStyle: {

    textAlign: 'center',
    height: 40,
    width: '90%',
    borderWidth: 1,
    borderColor: '#4CAF50',
    borderRadius: 7,
    marginTop: 12
  },

  button: {

    width: '90%',
    height: 40,
    padding: 10,
    backgroundColor: '#4CAF50',
    borderRadius: 8,
    marginTop: 10
  },

  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },

});