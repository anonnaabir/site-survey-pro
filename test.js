// import React, { Component } from 'react';
// import { StyleSheet, FlatList, View, Image, Modal, Alert, TouchableOpacity} from 'react-native';
// import { Container, Content, Text, Button, Icon, Item, Input, Footer, FooterTab} from 'native-base';
// import AsyncStorage from '@react-native-community/async-storage';
// import ImagePicker from 'react-native-image-crop-picker';
// import { ListItem } from 'react-native-elements';
// import RNHTMLtoPDF from 'react-native-html-to-pdf';
// import Share from 'react-native-share';
// import Realm from 'realm';
// let realm;

// export default class DT extends Component {
//   constructor(props) {
//     super(props);
//     //this.checkRecentProject.bind(this),
//     //this.loadDatas(),
//     this.getProjectName(),
//     //this.getSettings(),

//     this.array = [
//     ],

//       this.state = {
//         arrayHolder: [],
//         PlaceName: '', LocationName: '', myImage: '', myImageData: '',
//         filePath: '',
//         modalVisible: false,
//         projectModal: true,
//         recentModal: true,
//         projectName: null,
//         projectNote: '',
//         flotactive: false,
//         name: 'Ayan',
//         prname: null,
//         projectNameInput: 'Project Name',
//       }
      
//   }

  
//   getSettings = async () => {
//     try {
//     const { navigation, route } = this.props;
//     const { passname } = route.params;
//       const value = await AsyncStorage.getItem('amarname');
//         this.setState({projectNameInput: value});
//         console.log('Data Received:' + value);
//     } catch (error) {
//       console.log('Data Read Error');
//     }
//   };


//   setModalVisible = (visible) => {
//     this.setState({ modalVisible: visible });
//   }

//   newProjectModal = (visible) => {
//     this.setState({ projectModal: visible });
//   }

//   recentProjectModal = (visible) => {
//     this.setState({ recentModal: visible });
//   }

//   componentDidMount() {
//     this.setState({ arrayHolder: [...this.array] })
//     //this.checkRecentProject();
//   }

//   addDatas = async () => {
//     try {
//     const { arrayHolder } = this.state;
//     this.array.push({
//       title : this.state.PlaceName,
//       location : this.state.LocationName,
//       image : this.state.myImage,
//       imageData : this.state.myImageData,
//     });
//     this.setState({ arrayHolder: [...this.array] })
//     await AsyncStorage.setItem('data', JSON.stringify(this.array));
//     Alert.alert(
//       'Done',
//       'Data Saved Successfully',
//       [
//         {text: 'OK', onPress: () => {this.setModalVisible(false);}},
//       ],
//       { cancelable: false }
//     )  
//       }
   
//       catch (e) {
//         console.log('Error')
//       }
//   }

//     loadDatas = async () => {
//     try {
//       const value = await AsyncStorage.getItem('data');
//       const allData = this.array;
//       if(value !== null) {
//         this.setState({ arrayHolder: JSON.parse(value) });
//         console.log(value);
//       }
//     } catch(e) {
//       console.log('Data Read Error');
//     }
//   }

//   clearData = async() => {
//     AsyncStorage.clear();
//     console.log('Data clear done');
// }

//   chooseImage = () => {
//     ImagePicker.openPicker({
//       width: 300,
//       height: 400,
//       cropping: true,
//       includeBase64: true,
//     }).then(image => {
//       this.onSelectedImage(image);
//       this.imageData(image);
//       //console.log(image.path);
//     });
//   }

//   captureImage = () => {
//     ImagePicker.openCamera({
//       width: 300,
//       height: 400,
//       cropping: true,
//       includeBase64: true,
//     }).then(image => {
//       this.onSelectedImage(image);
//       this.imageData(image);
//       console.log(image);
//     });
//   }

//   onSelectedImage = (image) => {
//     console.log(image.path);
//     this.setState({myImage: image.path});
//   }

//   imageData = (image) => {
//     console.log(image.data);
//     this.setState({myImageData: image.data});
//   }

//   ListEmpty = () => {
//     const { navigation} = this.props;  
//     const name = this.state.projectName;
//       console.log(name);
//       if (name == null) {
//       return (
//       <Container>
//         <Modal
//         animationType="slide"
//         visible={this.state.projectModal}
//         >
//         <Content contentContainerStyle={styles.modal}>
//         <Text
//         style={{
//             lineHeight: 30,
//             marginBottom: 10,
//             fontSize: 17
//           }}
//         >{this.state.projectNameInput}</Text>
//           <Item regular
//           style={{
//             marginBottom: 30,
//           }}
//           >
//             <Input
//             placeholder="Enter Project Name"
//             onChangeText={(data) => this.setState({prname: data}) }
//             style={styles.textInputStyle}
//             underlineColorAndroid='transparent'
//             />
//           </Item>
//           <Text
//         style={{
//             lineHeight: 30,
//             marginBottom: 10,
//             fontSize: 17
//           }}
//         >Project Note (Optional)</Text>
//           <Item regular
//           style={{
//             marginBottom: 30,
//           }}
//           >
//             <Input
//             placeholder="Enter Project Note (If Any)"
//             onChangeText={data => this.setState({ projectNote: data })}
//             style={styles.textInputStyle}
//             underlineColorAndroid='transparent'
//             />
//           </Item>
//           <Button block
//           style={{
//             marginBottom: 10,
//           }}
//           onPress={() => {this.newProjectModal(false), this.recentProjectModal(false), this.setProjectName();}}
//           >
//             <Text>CREATE PROJECT</Text>
//           </Button>
//         </Content>
//         </Modal>
//       <Content contentContainerStyle={styles.content}>
//         <Text
//         style={{
//           textAlign: 'center',
//         }}
//       >Project Name: {this.state.projectName} Data not inserted yet. Please click the 'ADD DATA' button.</Text>
//         </Content>
//       </Container>
//     );
//   }

//     else {
//     return (
//       <Container>
//         <Modal
//         animationType="slide"
//         visible={this.state.recentModal}
//         >
//         <Content contentContainerStyle={styles.content}>
//         <Text
//         style={{
//           textAlign: 'center',
//         }}
//       >It seems that you have a previous project running. Do you want to continue?</Text>
//         <Button
//         onPress={() => {this.recentProjectModal(false), this.loadDatas()}}
//         >
//           <Text>Yes, I want to continue previous project.</Text>
//         </Button>
//         <Button
//         onPress={() => {this.clearData(), navigation.navigate("Site Survey Pro")}}
//         >
//           <Text>No, I want to create new project.</Text>
//         </Button>
//         </Content>
//         </Modal>
//         <Content contentContainerStyle={styles.content}>
//         <Text
//         style={{
//           textAlign: 'center',
//         }}
//       >Project Name: {this.state.projectName} Data not inserted yet. Please click the 'ADD DATA' button.</Text>
//         </Content>
//       </Container>
//     );
//       }
//   };


//   imageData = (image) => {
//     console.log(image.data);
//     this.setState({myImageData: image.data});
//   }
  
//     createPDF = async() => {
//       const { arrayHolder } = this.state;
//       let reportOutput = arrayHolder.map((i) => {
//         return `
//         <tr>
//         <td>${i.title}</td>
//         <td><img src=${`data:image/jpg;base64,${i.imageData}`} /></td>
//         </tr>
//         `;
//      });
      
//       const pdfHeader = "<h1>Project Name: " + this.state.projectName + "</h1>";
//       let options = {
//         html:
//       `${pdfHeader}
//       <style>
//       table.minimalistBlack {
//         border: 3px solid #000000;
//         width: 100%;
//         text-align: center;
//         border-collapse: collapse;
//       }
//       table.minimalistBlack td, table.minimalistBlack th {
//         border: 1px solid #000000;
//         padding: 5px 4px;
//       }
//       table.minimalistBlack tbody td {
//         font-size: 15px;
//       }
//       table.minimalistBlack thead {
//         background: #CFCFCF;
//         background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
//         background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
//         background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
//         border-bottom: 3px solid #000000;
//       }
//       table.minimalistBlack thead th {
//         font-size: 18px;
//         font-weight: bold;
//         color: #000000;
//         text-align: center;
//       }
//       table.minimalistBlack tfoot td {
//         font-size: 13px;
//       }
//           </style>
//                 <table class="minimalistBlack">
//                 <thead>
//                 <tr>
//                 <th>Note</th>
//                 <th>Image</th>
//                 </tr>
//                 </thead>
//                 <tbody>
//                   ${reportOutput}
//                 </tbody>
//               </table>
//       `,
//         fileName: this.state.projectName + '_report',
//         directory: 'docs',
//       };
   
//       let file = await RNHTMLtoPDF.convert(options);
//       console.log("file://" + file.filePath);
//       this.setState({filePath: "file://" + file.filePath});
//       Alert.alert(
//         'Awesome!',
//         'Report created successfully. Do you want to share this now?',
//         [
//           {text: 'Yes', onPress: () => this.shareFile()},
//           {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
//         ],
//         {cancelable: false},
//       );
//     }


//     setProjectName = async () => {
//       try {
//         const prname = this.state.prname;
//         this.setState({ projectName: prname });
//         await AsyncStorage.setItem('name', prname);
//         console.log("Project Name Saved");
//       } catch (e) {
//        console.log('Data Save Error');
//       }
//     }

//     getProjectName = async () => {
//       try {
//         const value = await AsyncStorage.getItem('name');
//         if(value !== null) {
//           this.setState({projectName: value});
//         }
//       } catch(e) {
//         console.log('Data Read Error');
//       }
//     }

//     checkRecentProject = () => {
//       const { projectName } = this.state;
//       if (projectName !== null) {
//         this.setState({projectModal: trues});
//       }
      
//       else {
//         this.setState({projectModal: true});
//       }
//     }

//     shareFile = () => {
//       const {filePath} = this.state;
//       if (filePath !== '') {
//         Share.open({
//           subject: 'Report Done',
//           message: 'The project is ready. Here is the report.',
//           url: this.state.filePath,
//         });
//         console.log('Share Done');
//       }

//       else {
//         Alert.alert(
//           'Wait',
//           'You need to save the report first before Email. Please save it.',
//           [
//             {text: 'OK'},
//           ],
//           { cancelable: false }
//         )
//       }
      
//     }

//   render() {
//     const { navigation,route } = this.props;
//     return (
//       <Container>
//         <Modal
//         animationType="slide"
//         visible={this.state.modalVisible}
//         >
//            <Content contentContainerStyle={styles.modal}>
//         <Text
//         style={{
//             lineHeight: 30,
//             marginBottom: 10,
//             fontSize: 17
//           }}
//         >Name</Text>
//           <Item regular
//           style={{
//             marginBottom: 30,
//           }}
//           >
//             <Input
//             placeholder="Enter Name"
//             onChangeText={data => this.setState({ PlaceName: data })}
//             style={styles.textInputStyle}
//             underlineColorAndroid='transparent'
//             />
//           </Item>
//           <Text
//         style={{
//             lineHeight: 30,
//             marginBottom: 10,
//             fontSize: 17
//           }}
//         >Location</Text>
//           <Item regular
//           style={{
//             marginBottom: 30,
//           }}
//           >
//             <Input
//             placeholder="Enter Name"
//             onChangeText={data => this.setState({ LocationName: data })}
//             style={styles.textInputStyle}
//             underlineColorAndroid='transparent'
//             />
//           </Item>
//           <Button block
//           style={{
//             marginBottom: 10,
//           }}
//           onPress={this.chooseImage}
//           >
//             <Text>CHOOSE IMAGE</Text>
//           </Button>
//           <Button block
//           style={{
//             marginBottom: 10,
//           }}
//           onPress={this.captureImage}
//           >
//             <Text>CAPTURE IMAGE</Text>
//           </Button>
//           <Button block success
//           style={{
//             marginBottom: 10,
//           }}
//           onPress={this.addDatas}
//           >
//             <Text>SAVE DATA</Text>
//           </Button>
//           <Button block success
//           style={{
//             marginBottom: 10,
//           }}
//           onPress={this.saveData}
//           >
//             <Text>STORE DATA</Text>
//           </Button>
//           <Button block danger
//           style={{
//             marginBottom: 10,
//           }}
//           onPress={() => {this.setModalVisible(false);}}
//           >
//             <Text>CANCEL INPUT</Text>
//           </Button>
//         </Content>
//         </Modal>
//         {/* <Text>Project Name is {this.state.projectName} </Text> */}
//           <Content>
//         <FlatList
//           data={this.state.arrayHolder}
//           // width='100%'
//           extraData={this.state.arrayHolder}
//           keyExtractor={(item, index) => index.toString()}
//           ListEmptyComponent={this.ListEmpty}
//           renderItem={({ item }) => 
//           <ListItem
//           title={item.title}
//           titleStyle={{ fontWeight: 'bold', fontSize: 18 }}
//           subtitle={item.location}
//           subtitleStyle={{ fontSize: 15 }}
//           leftAvatar={{ rounded: true, size: 'large', source: { uri: item.image } }}
//           bottomDivider
//           chevron
//           style={{
//             marginTop: 10,
//           }}
//           />
//         }
//         />
//         </Content>
        

//         <Footer>
//           <FooterTab>
//             <Button vertical
//             onPress={this.createPDF}
//             >
//               <Icon name="download" />
//               <Text>Make Report</Text>
//             </Button>
//             <Button vertical
//             onPress={() => {this.setModalVisible(true);}}
//             >
//               <Icon name="folder" />
//               <Text>Add Data</Text>
//             </Button>
//             <Button vertical
//             onPress={this.shareFile}
//             >
//               <Icon name="mail" />
//               <Text>Email Report</Text>
//             </Button>
//           </FooterTab>
//         </Footer>

//       </Container>
//     );
//   }
// }

// const styles = StyleSheet.create({
//     content: {
//       justifyContent: 'center',
//       alignItems: 'center',
//       flex:1,
//       paddingLeft: 20,
//       paddingRight: 20
//     },

//     modal: {
//       justifyContent: 'center', //Centered vertically
//       flex:1,
//       paddingLeft: 50,
//       paddingRight: 50,
      
//     },

//     addNewButton:{
 
//       position: 'absolute',
//       width: 50,
//       height: 50,
//       alignItems: 'center',
//       justifyContent: 'center',
//       right: 30,
//       bottom: 30,
//     },

//     createPDFButton:{
 
//       position: 'absolute',
//       width: 50,
//       height: 50,
//       alignItems: 'center',
//       justifyContent: 'center',
//       left: 30,
//       bottom: 30,
//     },
   
//     FloatingButton: {
//       resizeMode: 'contain',
//       width: 50,
//       height: 50,
//     }
//   });