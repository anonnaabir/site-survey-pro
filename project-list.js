import React, { Component } from 'react';
import { FlatList, StyleSheet, Modal, TouchableOpacity, View, Alert} from 'react-native';
import { Container, Content, Text, Button, List, ListItem, Left, Right, Icon, Footer, FooterTab} from 'native-base';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'appdata.db' });

export default class ProjectList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allProjects: [],
    };

    this.readData();
  }

  // componentDidMount() {
  //   this.setState({allProjects: this.deletedData});
  // }


  readData = () => {
    db.transaction(tx => {
      tx.executeSql("SELECT * FROM sqlite_master WHERE type='table' AND name NOT LIKE 'android_%' AND name NOT LIKE 'sqlite_%'", [], (tx, results) => {
        var data = [];
        for (let i = 0; i < results.rows.length; ++i) {
          data.push(results.rows.item(i));
          console.log('Read Done');
        }
        this.setState({allProjects: [...data]});
      });
    });
  }
  

  
  ListEmpty = () => {
    return(
          <Container>
            <Content contentContainerStyle={styles.content}>
              <Text style={{textAlign: 'center',}}>No Project Found. Please Create One First.</Text>
            </Content>
          </Container>
    )
  }


  render() {
    const { navigation} = this.props;
    return (
      <Container>
      <Content>
        <FlatList
          data={this.state.allProjects}
          // width='100%'
          extraData={this.state.allProjects}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={this.ListEmpty}
          renderItem={({ item }) =>
          <List>
            <ListItem noIndent
            style={{ backgroundColor: "#deffb7" }} 
            button
            onLongPress={ () =>
              Alert.alert(
                'Are You Sure!',
                'Do you want to delete this project?',
                [
                  {text: 'Yes', onPress: () => {

                    db.transaction(tx => {
                      tx.executeSql('DROP TABLE ' + item.name, [], (tx, results) => {
                        var data = [];
                        for (let i = 0; i < results.rows.length; ++i) {
                          data.push(results.rows.item(i));
                          console.log('Delete Done');
                        }
                        this.setState({allProjects: []});
                        console.log('Data Updated');
                        console.log('Navigated');
                        this.readData();
                      });
                    });
          
                  }},
                  {text: 'No'},
                ],
                {cancelable: false},
              )
            
            }
            onPress={() =>
              {
                const { navigation} = this.props;
                navigation.navigate('Load Data', {oldprojectName: item.name,});
              }
            }
            
            >
            <Left>
                <Text>{item.name.replace(/_/g, ' ')}</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" 
                style={{
                  color: '#2B2D2F',
                }}
                />
              </Right>
            </ListItem>
          </List>
        }/>
        </Content>

        <Footer>
          <FooterTab
          style={{
            backgroundColor: '#87C540',
          }}
          >
            <Button vertical
            onPress={() => navigation.navigate("Site Survey Pro")}
            >
              <Icon name="home" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Home</Text>
            </Button>
            <Button vertical
            onPress={() => navigation.navigate("Input Data")}
            >
              <Icon name="folder" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Create Project</Text>
            </Button>
            <Button vertical
            onPress={() => navigation.navigate("Settings")}
            >
              <Icon name="power" 
              style={{
                color: '#FFFFFF',
              }}
              />
              <Text
              style={{
                color: '#FFFFFF',
              }}
              >Settings</Text>
            </Button>
          </FooterTab>
        </Footer>

        </Container>
    );
  }
}


const styles = StyleSheet.create({
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
    paddingLeft: 20,
    paddingRight: 20
  },

});